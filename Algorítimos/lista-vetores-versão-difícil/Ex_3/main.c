#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main()
{
	char* A[10];
	char* B[10];
	bool* repeated = malloc(10);
	int i, j, k;

	for (i = 0; i < 10; i++) {
		printf("Escreva o valor para a posição %d do vetor: ", i);
		A[i] = malloc(50);
		fgets(A[i], sizeof(A[i]), stdin);
	}

	k = 0;
	for (i = 0; i < 10; i++) {
		if (!repeated[i]) {
			B[k] = malloc(sizeof(A[i]));
			B[k] = A[i];
			printf("%s", B[k]);
			k++;
			for (j = i + 1; j < 10; j++) {
				if (!strcmp(A[i], A[j]) && !repeated[i]) {
					repeated[j] = true;
				}
			}
		}
	}

	return 0;
}
