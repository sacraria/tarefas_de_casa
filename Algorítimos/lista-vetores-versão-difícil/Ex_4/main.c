#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void
readVector(char whichOne, char** vector, unsigned short sizeVector)
{
	int i;
	// for (i = 0; i < sizeVector; i++) {
	for (i = 0; i < sizeVector; i++) {
		printf("Escreva o valor para a posição %d do vetor %c: ",
		       i,
		       whichOne);
		vector[i] = malloc(50);
		fgets(vector[i], sizeof(vector[i]), stdin);
	}
}

int
main()
{
	char* A[10];
	char* B[20];
	bool* repeated = malloc(10);
	int i, j, k;

	readVector('A', &*A, 10);
	readVector('B', &*B, 20);

	for (i = 0; i < 10; i++) {
		if (!repeated[i]) {
			for (j = i + 1; j < 10; j++) {
				if (!strcmp(A[i], A[j]) && !repeated[i]) {
					repeated[j] = true;
					// printf("posição %d marcada como
					// repetida\n", j);
				}
			}
		}
	}

	k = 0;
	char* C[10];

	for (i = 0; i < 10; i++) {
		for (j = 0; j < 20; j++) {
			if (!strcmp(A[i], B[j]) && !repeated[i]) {
				C[k] = malloc(sizeof(A[i]));
				C[k] = A[i];
				printf("%s\n", C[k]);
				break;
			}
		}
	}
	return 0;
}
