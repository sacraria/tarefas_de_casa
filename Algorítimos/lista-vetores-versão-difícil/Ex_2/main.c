#include <stdio.h>
#include <stdlib.h>

void
inputInt(int* num)
{
	char* input = malloc(100);
	fgets(input, 100, stdin);
	*num = atoi(input);
	free(input);
}

int
main()
{
	int quantiaQuestoes;
	int i, acertos = 0;
	printf("Digite a quantia de questões: ");
	inputInt(&quantiaQuestoes);
	char* gabarito = malloc(quantiaQuestoes);
	// char gabarito[quantiaQuestoes];
	char* chutesAluno = malloc(quantiaQuestoes);

	for (i = 0; i < quantiaQuestoes; i++) {
		printf("Digite a resposta do gabarito para a questão %d: ",
		       i + 1);
		// chutesAluno[i] = getchar();
		scanf("%c", &gabarito[i]);
		while (getchar() != '\n')
			;
		printf("gabarito[i] = %c\n", gabarito[i]);
	}

	for (i = 0; i < quantiaQuestoes; i++) {
		printf("Digite a resposta do aluno para a questão %d: ", i + 1);
		// chutesAluno[i] = getchar();
		scanf("%c", &chutesAluno[i]);
		while (getchar() != '\n')
			;
		if (chutesAluno[i] == gabarito[i]) {
			acertos += 1;
		}
	}

	printf("O alunos acertou %d questões dentro das %d\n",
	       acertos,
	       quantiaQuestoes);
}
