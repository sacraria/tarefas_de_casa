#include <stdio.h>
#include <stdlib.h>

int
main()
{
	double* vetor = malloc(20 * sizeof(double));
	char inputStr[100];
	int i;
	double soma = 0;
	for (i = 0; i < 20; i++) {
		printf("Digite o %dº número: ", i + 1);
		fgets(inputStr, sizeof(inputStr), stdin);
		vetor[i] = atoi(inputStr);
		if (vetor[i] >= 10) {
			// printf("vetor[i] -> %lf\n", vetor[i]);
			soma += vetor[i];
		}
		if (vetor[i] < 0) {
			i = 20;
		}
	}
	printf("Soma -> %lf\n", soma);
}
