#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

void readVector(char** vector, char wichVector) {
	int i;
	for (i = 0; i < 10; i++) {
		vector[i] = malloc(50);
		printf("Digite o valor para o posição %d do vetor %c: ", i, wichVector);
		fgets(vector[i], 50, stdin);
		// printf("%s\n", vector[i]);
	}
}

int main() {
	int i, j;
	char* A[10];
	char* B[10];
	readVector(A, 'A');
	readVector(B, 'B');
	char* C[20];
	// for (i = 0; i < 10; i++) {
	// 	C[i] = A[i];
	// }
	for (i = 0; i < 10; i++) {
		C[i + 10] = B[i];
	}
	bool* repeated = (bool*) calloc(20, sizeof(bool));
	for (i = 0; i < 20; i++) {
		if (!repeated[i] && i < 19) {
			for (j = i + 1; j < 20; j++) {
				if (!strcmp(C[i], C[j])) {
					repeated[j] = true;
				}
			}
		}
	}
	j = 0;
	for (i = 0; i < 20; i++) {
		if (!repeated[i]) {
			C[j] = C[i];
			j++;
			printf("%s\t", C[j]);
		}
	}
}
