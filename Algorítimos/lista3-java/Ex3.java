import java.util.Scanner;

public class Ex3 {
   public static void main(String args[]) {

      Scanner teclado = new Scanner(System.in);

      int quantiaNegativos = 0, quantiaPositivos = 0;
      double num;

      for (int i = 1; i <= 10; i++) {
         System.out.print("Digite o número " + i + ": ");
         num = teclado.nextDouble();

         if (num < 0) {
            quantiaNegativos++;
         } else {
            quantiaPositivos++;
         }

      }

      System.out.println("A quantia de positivos: " + quantiaPositivos);
      System.out.println("A quantia de negativos: " + quantiaNegativos);

      teclado.close();
   }
}
