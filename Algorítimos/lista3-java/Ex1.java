import java.util.Scanner;

public class Ex1 {
   public static void main(String args[]) {
      Scanner sc = new Scanner(System.in);
      int i, num;
      System.out.print("Digite o número -> ");
      num = sc.nextInt();
      i = 1;
      while (i <= num) {
         System.out.println(i);
         i++;
      }
      sc.close();
   }
}
