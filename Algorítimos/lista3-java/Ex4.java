import java.util.Scanner;

public class Ex4 {
   public static void main(String args[]) {
      double divisor, num;
      int count = 0;
      Scanner sc = new Scanner(System.in);
      System.out.printf("Digite o número à se dividir: ");
      divisor = sc.nextDouble();

      for (int i = 1; i <= 5; i++) {
         System.out.printf("Digite o número %d à se verificar a divisibilidade: ", i);
         num = sc.nextDouble();
         if (num % divisor == 0) {
            count++;
            System.out.println(num + " é divisível por " + divisor + "!!");
         } else {
            System.out.println(num + " não é divisível por " + divisor + ":(");
         }
      }

      System.out.println(count + " desses números são divisíveis por " + divisor + "!");

      sc.close();
   }
}
