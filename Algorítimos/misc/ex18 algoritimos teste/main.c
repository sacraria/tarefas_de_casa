#include <stdio.h>

int main() {
   int aulasTotais,faltas;
   float notaFinal;

   printf("Digite o número total de aulas: ");
   scanf("%i", &aulasTotais);

   printf("Digite o número de faltas do aluno: ");
   scanf("%i", &faltas);

   if (faltas > aulasTotais * 0.25) {
      printf("Reprovado por faltas\n");
      return 1;
   }

   printf("Digite a nota final do aluno: ");
   scanf("%f", &notaFinal);

   if ((int)(notaFinal * 10) % 5 != 0) {
      printf("A nota deve set múltipla de 0.5\n");
      return 1;
   } else if (notaFinal > 10 || notaFinal < 0) {
      printf("A nota deve estar entre 0 e 10");
   } else if (notaFinal > 5) {
      printf("Aprovado\n");
   } else if (notaFinal == 4 || notaFinal == 4.5) {
      printf("Segunda época\n");    
   } else {
      printf("Reprovado\n");
   }

   return 0;
}
