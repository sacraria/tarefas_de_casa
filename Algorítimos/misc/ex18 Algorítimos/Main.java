import java.util.Scanner;

class Main {
	public static void main(String[] args) {
   	Scanner sc = new Scanner(System.in);

   	System.out.printf("Escreva a nota final do aluno: ");
   	float nota = sc.nextFloat();

   	if (nota > 10 || nota < 0) {
      	System.out.printf("A nota deve estar entre 0 e 10!\n");
      	System.exit(0); 
      } else if (nota % 0.5 != 0) {
      	System.out.printf("A nota deve ser múltipla de 0.5!\n");
      	System.exit(0); 
      }

   	System.out.printf("Escreva a quantia total de aulas: ");
   	int aulasTotal= sc.nextInt();

   	System.out.printf("Escreva a quantia total de faltas do aluno: ");
   	float faltas = sc.nextInt();

   	if (aulasTotal * 0.25 < faltas) {
      	System.out.printf("Reprovado por faltas!\n");
      	System.exit(0);
   	} else if (nota >= 5) {
      	System.out.printf("Aprovado!\n") ;
   	} else if (nota == 4.5f || nota == 4.0f) {
      	System.out.printf("Segunda época\n");
   	} else {
      	System.out.printf("Reprovado\n");
   	}
   	sc.close();
	}
}
