#include <stdio.h>
#include <math.h>

int main() {
	int entrada, quantidadeAlgarismos, soma = 0;
	printf("Digite o número para converter da base8 para a base10: ");
	scanf("%d", &entrada);
	for (quantidadeAlgarismos = 0; entrada / pow(10, quantidadeAlgarismos) >= 10; quantidadeAlgarismos++);
	while (quantidadeAlgarismos >= 0) {
		soma += entrada / (int)pow(10, quantidadeAlgarismos) * (int)pow(8, quantidadeAlgarismos);
		entrada %= (int)pow(10, quantidadeAlgarismos);
		quantidadeAlgarismos--;
	}
	printf("%d\n", soma);
}
