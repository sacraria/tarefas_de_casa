#include <stdio.h>

int main() {
	int num, i, sum = 0;
	printf("Digite o número aí vá: ");
	scanf("%d", &num);
	for (i = 1; i < num; i++) {
		if (num % i == 0) {
			sum += i;
			// printf("%d\n", sum);
		}
	}
	// printf("%d\n", sum);
	if (sum == num) {
		printf("O número %d é um quadrado perfeito\n", num);
	} else {
		printf("O número %d não é um quadrado perfeito\n", num);
	}
	return 0;
}
