#include <stdio.h>

int main() {
	float peso, altura;
	int lowCount = 0;
	for (int i = 1 ; i <= 2; i++) {
		printf("Digite o peso da pessoa %d: ", i);
		scanf("%f", &peso);
		printf("Digite a altura da pessoa %d: ", i);
		scanf("%f", &altura);
		if (peso / (altura * altura) <= 18.5) {
			lowCount += 1;
		}
	}
	printf("%d pessoas estão abaixo do peso\n", lowCount);
}
