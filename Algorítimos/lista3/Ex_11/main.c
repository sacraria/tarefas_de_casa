#include <stdio.h>

int main() {
	int input, i, resultado = 0;
	printf("Digite o número -> ");
	scanf("%d", &input);
	for (i = 0; i <= input; i++) {
		if (i % 2 == 0) {
			resultado = resultado + i;
		}
	}
	printf("%d\n", resultado);
}
