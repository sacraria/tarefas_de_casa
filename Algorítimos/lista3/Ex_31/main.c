#include <stdio.h>
#include <stdlib.h>

int input() {
	char str[50];
	int num;
	while (1) {
		fgets(str, sizeof(str), stdin);
		num = atoi(str);
		if (num <= 0) {
			printf("Digite um número maior que zero!!\n");
		} else {
			return num;
		}
	}
}

int main() {
	int num1, num2, switchNum, i;

	printf("Digite o primeiro número: ");
	num1 = input();
	printf("Digite o segundo número: ");
	num2 = input();
	printf("Digite a quantia de vezes a se calcular: ");
	i = input();

	printf("%d ", num1);
	while (i > 0) {
		printf("%d ", num2);
		switchNum = num1 + num2;
		num1 = num2;
		num2 = switchNum;
		i--;
	}
	printf("\n");
}
