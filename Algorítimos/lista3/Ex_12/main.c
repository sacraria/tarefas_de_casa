#include <stdio.h>

int main() {
	int positiveAmount = 0, negativeAmount = 0;
	double input;
	for (int i = 0; i < 20 ; i++) {
		printf("Digite o número %d -> ", i + 1);
		scanf("%lf", &input);
		if (input < 0) {
			negativeAmount = negativeAmount + 1;
		} else {
			positiveAmount = positiveAmount + 1;
		}
	}
	printf("Quantia de positivos -> %d\n", positiveAmount);
	printf("Quantia de negativos -> %d\n", negativeAmount);
}
