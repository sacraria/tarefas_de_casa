#include <stdio.h>
#include <stdlib.h>

int main() {
	int input, before, after;

	char inputStr[100];
	printf("Digite o número à se calcular o fibonnacci: ");
	while (1) {
		fgets(inputStr, sizeof(inputStr), stdin);
		input = atoi(inputStr);
		if (input <= 0) {
			printf("Digite um número maior que zero!");
		} else {
			break;
		}
	}
	printf("%d ", input);
	before = input;
	after = input;
	for (int i = 0; i < 20; i++) {
		printf("%d ", after);
		input = after + before;
		before = after;
		after = input;
	}
	printf("\n");
}
