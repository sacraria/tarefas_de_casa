#include <stdio.h>
#include <math.h>

int main() {
	int series, switchOperators = 0, divisor = 1;
	double operation;
	printf("Digite a a precisão do pi -> ");
	scanf("%d", &series);

	while (series > 0) {
		if (switchOperators == 0) {
			operation += 1.0 / (divisor * divisor * divisor);
			switchOperators = 1;
		} else {
			operation -= 1.0 / (divisor * divisor * divisor);
			switchOperators = 0;
		}
		divisor += 2;
		series --;
	}
	// printf("%lf\n", operation);

	// operation = 32 * operation;
	operation = cbrt(32.0 * operation);
	printf("%lf\n", operation);
}
