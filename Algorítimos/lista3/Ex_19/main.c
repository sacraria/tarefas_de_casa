#include <stdio.h>

int main() {
	float massa;
	int tempo = 0;
	printf("Digite a massa -> ");
	scanf("%f", &massa);
	while (massa > 0.05) {
		massa = massa / 2;
		tempo += 50;
	}
	printf("tempo (em horas) -> %d:%d\n", tempo / 60, tempo % 60);}
