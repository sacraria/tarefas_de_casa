#include <stdio.h>

int main() {
	int num;
	double sum;
	printf("Digite o número: ");
	scanf("%d", &num);

	for (int i = 2; i >= num ; i++) {
		if (i % 2 == 0) {
			sum = sum - 1.0 / i;
		} else {
			sum = sum + 1.0 / i;
		}
	}

	printf("%lf", sum);
	return 0;
}
