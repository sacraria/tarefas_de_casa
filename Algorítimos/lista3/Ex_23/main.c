#include <stdio.h>
#include <stdlib.h>

int input(char *str, char n) {
	int input;
	while (1) {
		printf("Digite o número %c à se descobrir o MDC: ", n);
		fgets(str, sizeof(str), stdin);
		input = atoi(str);
		if (input == 0) {
			printf("Não pode 0 ou caractére!");
		} else {
			return (input);
		}
	}
}

int main() {
	int i, j;
	char str1[50], str2[50];
	int input1 = input(str1, 1);
	int input2 = input(str2, 2);

	if (input1 >= input2) {
		i = input1 - 1;
	} else {
		i = input2 - 1;
	}

	while (i > 1) {
		if (input1 % i == 0 && input2 % i == 0 &&
		    (i != input1 && i != input2)) {
			printf("%d", i);
			return 0;
		}
		i--;
	}
	// printf("O número é primo\n");
}
