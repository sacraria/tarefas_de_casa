#include <stdio.h>
#include <stdlib.h>

int main() {
	unsigned int input, i, isPerfect = 0;
	char inputStr[50];
	while (1) {
		printf("Digite o número à se verificar a perfeição: ");
		fgets(inputStr, sizeof(inputStr), stdin);
		input = atoi(inputStr);
		if (input == 0) {
			printf("Não pode 0 ou caractére!\n");
		} else {
			break;
		}
	}

	for (i = 1; i < input; i++) {
		if (input % i == 0) {
			isPerfect = isPerfect + i;
		}
	}
	if (isPerfect == input) {
		printf("O número é perfeito!\n");
	} else {
		printf("O número não é perfeito\n");
	}
	return 0;
}
