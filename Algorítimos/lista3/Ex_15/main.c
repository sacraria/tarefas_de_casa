#include <stdio.h>

int main() {
	int num, div, i;
	printf("Escreva o número vá -> ");
	scanf("%d", &num);
	printf("Escreva o dividendo vá -> ");
	scanf("%d", &div);
	i = num / div;
	// for (j = i ; i < num; i += j);
	// printf("%d", i);
	const int j = i;
	while (i < num) {
		i = i + j;
		printf("i = %d\nj = %d\n", i, j);
	}
	num -= i;
	printf("%d", num);
}
