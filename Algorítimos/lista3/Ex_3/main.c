#include <stdio.h>
#include <stdbool.h>

int main() {
   long double num[10];
   bool isNegative[10];
   int i;
   for (i = 0; i < 10; i++) {
   	printf("Escreva o número %d -> ", i + 1);
   	scanf("%Lf", &num[i]);
   	if (num[i] < 0) {
      	isNegative[i] = 1;
   	} else {
      	isNegative[i] = 0;
   	}
   }
   for (i = 0; i < 10; i++) {
   	if (isNegative[i] == 1) {
			printf("o número %d, vulgo %Lf é negativo!!\n", i, num[i]);
   	}
   }

}
