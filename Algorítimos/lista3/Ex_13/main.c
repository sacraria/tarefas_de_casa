#include <stdio.h>

int main() {
	int input, i;
	scanf("%d", &input);
	for (i = input - 1; i > 1; i--) {
		input *= i;
	}
	printf("%d\n", input);
}
