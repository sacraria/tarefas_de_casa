#include <stdio.h>

int main() {
	int num1, num2, i;
	printf("Digite o primeiro número -> ");
	scanf("%d", &num1);
	printf("Digite o segundo número -> ");
	scanf("%d", &num2);
	const int constNum = num1;

	for (i = 1; i < num2 ; i++) {
		num1 = num1 + constNum;
	}
	printf("%d\n", num1);
}
