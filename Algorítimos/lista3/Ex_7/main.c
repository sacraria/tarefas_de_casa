#include <stdio.h>

int main() {
   double soma, input;
   while (1) {
      printf("Digitar aqui -> ");
      scanf("%lf", &input);
      if (input < 0) {
         break;
      }
      soma = soma + input;
   }
   printf("%lf\n", soma);
}
