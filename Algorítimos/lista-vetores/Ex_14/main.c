#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct argStuff {
    int *argc;
    char **argv;
    int currentYear;
    int *data;
    int lastData;
};

void showUsage()
{
    printf("Uso:\n");
    printf("-c\t--current-date\t'insira a data atual aqui'\n");
    printf("-d\t--date\t\t'insira as datas de vitória aqui (pelo menos 2)'\n");
}

void verifyCurrentDate(struct argStuff *argStuff)
{
    bool checker = false;
    int i;
    for (i = 1; i <= *argStuff->argc; i++) {
        if (strcmp(argStuff->argv[i], "-c") == 0 ||
            strcmp(argStuff->argv[i], "--current-date") == 0) {
            checker = true;
            i++;
            argStuff->currentYear = atoi(argStuff->argv[i]);
            if (argStuff->currentYear == 0) {
                showUsage();
                exit(1);
            }
            i = *argStuff->argc;
        }
    }
    if (checker == false) {
        showUsage();
        exit(1);
    }
}

void verifyDates(struct argStuff *argStuff)
{
    bool checker = false;
    int i, j = 0;
    for (i = 1; i <= *argStuff->argc; i++) {
        if (strcmp(argStuff->argv[i], "-d") == 0 ||
            strcmp(argStuff->argv[i], "--date") == 0) {
            checker = true;
            i += 1;
            while (i <= *argStuff->argc) {
                argStuff->data[j] = atoi(argStuff->argv[i]);
                if (argStuff->data[j] == '\0') {
                    argStuff->lastData = argStuff->data[j - 1];
                    return;
                }
                if (j > 0) {
                    if (argStuff->data[j] <= argStuff->data[j - 1]) {
                        printf("Abortado! as datas não "
                               "estão em ordem crescente\n");
                        exit(1);
                    }
                    printf("%d anos se passaram entre a "
                           "%d⁰ e a %d vitória\n",
                           argStuff->data[j] - argStuff->data[j - 1], j, j + 1);
                }
                i++;
                j++;
            }
            argStuff->lastData = argStuff->data[j - 1];
        }
    }
    if (checker == false) {
        showUsage();
        exit(1);
    }
}

int main(int argc, char *argv[])
{
    struct argStuff argStuff;
    argc--;
    argStuff.argc = &argc;
    argStuff.argv = &*argv;
    argStuff.data = malloc(argc * sizeof(int));
    if (argc < 5) {
        showUsage();
        return 1;
    }

    verifyCurrentDate(&argStuff);
    verifyDates(&argStuff);

    // printf("%d %d\n", argStuff.currentYear, argStuff.lastData);
    if (argStuff.currentYear < argStuff.lastData) {
        printf("Abortado! A data atual é menor que o último ano de "
               "vitória\n");
        return 1;
    }
    printf("Se passaram %d anos entre a última data e o ano atual\n",
           argStuff.currentYear - argStuff.lastData);
}