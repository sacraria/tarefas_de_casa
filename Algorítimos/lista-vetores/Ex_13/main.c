#include <stdio.h>

int main(int argc, char* argv[]) {
    int i = 1;
    int j = 20;
    char* troca;
    if (argc != 21) {
        printf("Digite 20 argumentos!\n");
        return 1;
    }
    while (i <= 10) {
        troca = argv[i];
        argv[i] = argv[j];
        argv[j] = troca;
        i++;
        j--;
    }

    for (i = 1; i <= 20; i++) {
        printf("%s\n", argv[i]);
    }

    return 0;
}