#include <stdio.h>

int main() {
    int vetor[20];
    int pares[20];
    int impares[20];
    int i, j, k;

    for (i = 0; i < 20; i++) {
        printf("Digite o %d⁰ número: ", i + 1);
        scanf("%d", &vetor[i]);
    }

    j = 0;
    k = 0;
    for (i = 0; i < 20; i++) {
        if (vetor[i] % 2 == 0) {
            pares[j] = vetor[i];
            printf("número: %d\tposição no vetor \"vetor\": %d\tposição no vetor \"pares\": %d\n", vetor[i], i, j);
            j++;
        } else {
            impares[k] = vetor[i];
            k++;
            printf("número: %d\tposição no vetor \"vetor\": %d\tposição no vetor \"impares\": %d\n", vetor[i], i, k);
        }
    }

    return 0;
}