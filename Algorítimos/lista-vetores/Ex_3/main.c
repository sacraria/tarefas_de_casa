#include <stdio.h>

int main() {
    double vetor[10];
    int i;
    double maior;
    double menor;

    for (i = 0; i < 10; i++) {
        printf("Escreva o %d⁰ número: ", i + 1);
        scanf("%lf", &vetor[i]);
    }

    for (i = 0; i < 10; i++) {
        if (i == 0) {
            maior = vetor[i];
            menor = vetor[i];
        } else {
            if (vetor[i] > maior) {
                maior = vetor[i];
            }
            if (vetor[i] < menor) {
                menor = vetor[i];
            }
        }
    }

    printf("O maior número é %lf\n", maior);
    printf("O menor número é %lf\n", menor);

    return 0;
}
