#include <stdio.h>

int main() {
    int vetor[10];
    int i;

    for (i = 0; i < 10; i++) {
        printf("Digite o número para a posição %d: ", i);
        scanf("%d", &vetor[i]);
    }

    printf("\nNúmeros pares: \n");
    for (i = 0; i < 10; i++) {
        if (vetor[i] % 2 == 0) {
            printf("%d\n", vetor[i]);
        }
    }

    printf("\nNúmeros ímpares: \n");
    for (i = 0; i < 10; i++) {
        if (vetor[i] % 2 != 0) {
            printf("%d\n", vetor[i]);
        }
    }

    return 0;
}