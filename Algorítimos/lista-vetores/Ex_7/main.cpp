#include <iostream>

int
main()
{
	std::string vetorStr;
	int vetor[50];
	std::cout << "Digite o número: ";
	std::cin >> vetorStr;

	try {
		vetor[0] = std::stoi(vetorStr);
	} catch (std::invalid_argument e) {
		std::cout << "Digite um número válido\n";
		return 1;
	}

	for (int i = 1; i < 50; i++) {
		vetor[i] = vetor[i - 1] * 3;
		std::cout << vetor[i] << "\n";
	}
}
