#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main(int argc, char* argv[])
{
	argc--;
	if (argc < 1) {
		printf("Pelos menos um argumento!\n");
		return 1;
	}
	int i;
	double* vetorA = malloc(argc);
	double* vetorK = malloc(argc);

	// nota para mim mesmo: porque caralhos eu achei q K era uma entrada?
	for (i = 0; i < argc; i++) {
		// printf("%s argv\n", argv[i]);
		if (strcmp(argv[i + 1], "A") == 0) {
			i++;
			while (i < argc) {
				if ((vetorA[i] = atoi(argv[i + 1])) == 0) {
					i--;
					break;
				}
				vetorK[i] = vetorA[i] * 3;
				printf("%lf\n", vetorK[i]);
				i++;
			}
		} else {
			printf("Algo de errado não está certo\n");
			free(vetorA);
			free(vetorK);
			return 1;
		}
		free(vetorA);
		free(vetorK);
	}
	return 0;
}