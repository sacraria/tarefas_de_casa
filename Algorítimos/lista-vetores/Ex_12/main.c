#include <stdio.h>
#include <stdlib.h>

int main() {
    float quantiaProdutos;
    int i;
    int run;
    int maior;
    printf("Digite o número de produtos: ");
    scanf("%f", &quantiaProdutos);

    if (quantiaProdutos < 3) {
        printf("Precisa-se de pelo menos 3 produtos\n");
        return 1;
    }

    float *preco = malloc(quantiaProdutos * sizeof(float));
    for (i = 0; i < quantiaProdutos; i++) {
        printf("Digite o valor do %d⁰ produto: ", i + 1);
        scanf("%f", &preco[i]);
    }

    for (run = 1; run <= 3; run++) {
        maior = 0;
        for (i = 1; i < quantiaProdutos; i++) {
            if (preco[i] > preco[maior]) {
                maior = i;
            }
        }
        printf("Em %d⁰ lugar, o %d⁰ produto, com o preço de R$%.2f\n", run, maior + 1, preco[maior]);
        preco[maior] = 0;
    }
    free(preco);
    return 0;
}