#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int
main(int argc, char* argv[])
{
	argc--;
	if (argc < 1) {
		printf("Dê pelo menos um argumento\n");
		return 1;
	}
	// printf("%d\n", argc);
	// printf("%s\n", argv[0]);
	int vetor[argc];
	int i;
	for (i = 0; i < argc; i++) {
		vetor[i] = atoi(argv[i + 1]);
		if (vetor[i] == 0) {
			vetor[i] = 1;
		}
	}

	for (i = 0; i < argc; i++) {
		printf("%d ", vetor[i]);
	}
	printf("\n");

	return 0;
}