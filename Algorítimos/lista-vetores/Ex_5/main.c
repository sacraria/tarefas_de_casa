#include <stdio.h>

int main() {
    int vetor[10];
    int i;
    int quantiaNegativos = 0;

    for (i = 0; i < 10; i++) {
        printf("Digite o número para a posição %d: ", i);
        scanf("%d", &vetor[i]);
    }

    for (i = 0; i < 10; i++) {
        if (vetor[i] < 0) {
            quantiaNegativos++;
            printf("Na posição %d, o número é negativo\n", i);
        }
    }

    printf("%d números são negativos\n", quantiaNegativos);

    return 0;
}
