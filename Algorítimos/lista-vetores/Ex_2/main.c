#include <stdio.h>

int main(void) {
    int vetor[6];
    int i;

    for (i = 0; i < 6; i++) {
        printf("Digite o %d⁰ número: ", i + 1);
        scanf("%d", &vetor[i]);
    }

    for (i = 5; i >= 0; i--) {
        printf("%d\n", vetor[i]);
    }

    return 0;
}