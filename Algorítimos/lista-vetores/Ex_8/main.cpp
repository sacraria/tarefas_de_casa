#include <iostream>

void
input(float* input)
{
	std::string inputStr;
	std::cin >> inputStr;
	while (true) {
		try {
			*input = std::stof(inputStr);
			break;
		} catch (std::error_code e) {
			std::cout << "Digite um número válido!\n";
			exit(1);
		}
	}
}

int
main()
{
	float media;
	float temp;
	int quantiaAlunos;
	int i;
	std::cout << "digite a média: ";
	input(&media);
	std::cout << "Digite a quantia de alunos: ";
	input(&temp);
	quantiaAlunos = (int)temp;
	float nota[quantiaAlunos];

	for (i = 0; i < quantiaAlunos; i++) {
		std::cout << "Digite a nota do " << i + 1 << "⁰ aluno: ";
		input(&nota[i]);
	}

	for (i = 0; i < quantiaAlunos; i++) {
		if (nota[i] > media) {
			std::cout << nota[i] << " está acima da média\n";
		}
	}
}