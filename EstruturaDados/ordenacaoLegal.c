#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[]) {
	// o argumento do comando conta como um argv, então consideramos
	// como argumento a partir do argv[1]
	int tamanhoVetor = argc > 1 ? atoi(argv[1]) : 10;

	int vetor[tamanhoVetor];

	// preencher e mostrar o vetor
	srand(time(NULL));
	printf("Antes:\n[");
	for (int i = 0; i < tamanhoVetor; i++) {
		// número entre 0 e argv[2] ou 100
		vetor[i] = rand() % (argc > 2 ? atoi(argv[2]) + 1 : 101);

		printf("%d%s", vetor[i], i < tamanhoVetor - 1 ? ", " : "");
	}
	printf("]\n\n");

	// ordenação
	int indexMaior = tamanhoVetor - 1;
	for (int indexMenor = 0; indexMenor < indexMaior; indexMenor++, indexMaior--) {
		int maior = indexMenor;
		int menor = indexMenor;
		for (int i = indexMenor + 1; i <= indexMaior; i++) {
			if (vetor[i] < vetor[menor]) menor = i;
			if (vetor[i] > vetor[maior]) maior = i;
		}
		// trocar menor
		int aux = vetor[menor];
		vetor[menor] = vetor[indexMenor];
		vetor[indexMenor] = aux;
		// adequar o maior se ele for o primeiro índice
		if (maior == indexMenor) maior = menor;

		// trocar maior
		aux = vetor[maior];
		vetor[maior] = vetor[indexMaior];
		vetor[indexMaior] = aux;
	}

	// mostrar o vetor ordenado
	printf("Depois:\n[");
	for (int i = 0; i < tamanhoVetor; i++) {
		printf("%d%s", vetor[i], i < tamanhoVetor - 1 ? ", " : "");
	}
	printf("]\n");

	return 0;
}
