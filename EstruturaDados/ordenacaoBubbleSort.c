#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[]) {
	// o argumento do comando conta como um argv, então consideramos
	// como argumento a partir do argv[1]
	int tamanhoVetor = argc > 1 ? atoi(argv[1]) : 10;

	int vetor[tamanhoVetor];

	// preencher e mostrar o vetor
	srand(time(NULL));
	printf("Antes:\n[");
	for (int i = 0; i < tamanhoVetor; i++) {
		// número entre 0 e argv[2] ou 100
		vetor[i] = rand() % (argc > 2 ? atoi(argv[2]) + 1 : 101);

		printf("%d%s", vetor[i], i < tamanhoVetor - 1 ? ", " : "");
	}
	printf("]\n\n");

	// ordenação
	clock_t comeco = clock();
	for (int indexMaior = tamanhoVetor - 1; indexMaior >= 0; indexMaior--) {
		for (int i = 0; i < indexMaior; i++) {
			if (vetor[i + 1] < vetor[i]) {
				int aux = vetor[i];
				vetor[i] = vetor[i + 1];
				vetor[i + 1] = aux;
			}
		}
	}
	clock_t fim = clock();
	double tempoGasto = (double)(fim - comeco)/CLOCKS_PER_SEC;

	// mostrar o vetor ordenado
	printf("Depois:\n[");
	for (int i = 0; i < tamanhoVetor; i++) {
		printf("%d%s", vetor[i], i < tamanhoVetor - 1 ? ", " : "");
	}
	printf("]\n");
	printf("tempo gasto -> %lf\n", tempoGasto);

	return 0;
}
