#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

du ~ 1> /dev/null 2> /dev/null

# for i in 1000 10000 100000 200000 400000 100000
for i in 1000000
do
	echo "com $i elementos no vetor: "
	echo ""
	$SCRIPT_DIR/rodar.sh $i 5000000
done
