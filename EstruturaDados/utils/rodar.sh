#!/bin/sh

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

cd $SCRIPT_DIR/../bin

if [[ $1 = "" ]] || [[ $2 = "" ]]
then
	echo "uso:"
	echo "1⁰ argumento -> tamanho do vetor"
	echo "2⁰ argumento -> números aleatórios de 0 ao argumento"
	exit
fi

for i in bubbleSort.run selectionSort.run ebaSort.run insertionSort.run
do
	echo "$i:"
	./$i $1 $2
	echo ""
done
# bubbleSort.run  ebaSort.run  legalSort.run  selectionSort.run
