#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

cd $SCRIPT_DIR/../bin

modo="otimizado"
if [[ $modo = "otimizado" ]]
then
	gcc $SCRIPT_DIR/../ordenacaoEba.c -o ebaSort.run -march=native -flto=8 -pipe -O3 -fuse-linker-plugin
	gcc $SCRIPT_DIR/../ordenacaoSelection.c -o selectionSort.run -march=native -flto=8 -pipe -O3 -fuse-linker-plugin
	gcc $SCRIPT_DIR/../ordenacaoBubbleSort.c -o bubbleSort.run -march=native -flto=8 -pipe -O3 -fuse-linker-plugin
	gcc $SCRIPT_DIR/../ordenacaoInsertion.c -o insertionSort.run -march=native -flto=8 -pipe -O3 -fuse-linker-plugin
else
# elif [[ $modo = "sem_otimizacao" ]]
	gcc $SCRIPT_DIR/../ordenacaoEba.c -o ebaSort.run
	gcc $SCRIPT_DIR/../ordenacaoSelection.c -o selectionSort.run
	gcc $SCRIPT_DIR/../ordenacaoBubbleSort.c -o bubbleSort.run
	gcc $SCRIPT_DIR/../ordenacaoInsertion.c -o insertionSort.run
fi
