#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

char letraParaValor(char letra) {
	return letra - 97;
}

char valorParaLetra(char valor) {
	return valor + 97;
}

char subtrairLetras(char letra1, char letra2) {
	letra1 -= letraParaValor(letra2);
	if (letra1 < 'a') return ('z' + 1) - ('a' - letra1);
	return letra1;
}

long long obterOcorrenciaDaLetra(char c) {
	// ocorrências para o português retiradas da wikipedia
	// https://pt.wikipedia.org/wiki/Frequ%C3%AAncia_de_letras
	// acabou funcionando bem para o inglês também
	switch (c) {
		case 'a': return 1463;
		case 'e': return 1257;
		case 'o': return 1073;
		case 's': return 7810;
		case 'r': return 6530;
		case 'i': return 6180;
		case 'n': return 5050;
		case 'd': return 5010;
		case 'm': return 4740;
		case 'u': return 4630;
		case 't': return 4340;
		case 'c': return 3880;
		case 'l': return 2780;
		case 'p': return 2520;
		case 'v': return 1670;
		case 'g': return 1300;
		case 'h': return 1280;
		case 'q': return 1200;
		case 'b': return 1040;
		case 'f': return 1020;
		case 'z': return 0470;
		case 'j': return 0400;
		case 'x': return 0210;
		case 'k': return 0020;
		case 'w': return 0010;
		case 'y': return 0010;
		default:
			printf("??????\n");
			return 0;
	}
}


long long testarOcorrenciaDeDadaLetraNoTexto(char* texto, int tamanhoChave, char chaveATestar) {
	int verificarLetra = 0;
	long long chanceDeSerEssaLetra = 0;
	while (*texto) {
		// vai cair nesse if na primeira iteração
		if (!verificarLetra) {
			chanceDeSerEssaLetra += obterOcorrenciaDaLetra(subtrairLetras(*texto, chaveATestar));
			verificarLetra = tamanhoChave - 1;
		} else {
			verificarLetra--;
		}
		texto++;
	}
	return chanceDeSerEssaLetra;
}

// texto -> ponteiro para o caractere inicial à se iniciar o teste
char quebrarUmaLetraDaChave(char* texto, int tamanhoChave) {
	long long chanceDeSer[26];
	for (char chaveATestar = 'a'; chaveATestar <= 'z'; chaveATestar++) {
		chanceDeSer[letraParaValor(chaveATestar)] = testarOcorrenciaDeDadaLetraNoTexto(texto, tamanhoChave, chaveATestar);
	}

	char maiorIndex = 0;
	for (char i = 1; i < 26; i++) {
		if (chanceDeSer[i] > chanceDeSer[maiorIndex]) {
			maiorIndex = i;
		}
	}

	return valorParaLetra(maiorIndex);
}

char* quebrarChaveAgoraDeVdd(char* str, int tamanhoChave) {
	// aparentemente o que é alocado na stack vai sumir
	// quando a função for terminada
	// então alocamos na heap
	char* chaveDecifrada = malloc(tamanhoChave + 1); // +1 -> null terminator

	char i = 0; 
	for (; i < tamanhoChave; i++) {
		chaveDecifrada[i] = quebrarUmaLetraDaChave(str + i, tamanhoChave);
	}
	chaveDecifrada[i] = '\0';

	return chaveDecifrada;

}

void decifrarTextoAPartirDaChave(char* texto, char* chave) {
	char* ciclagemChave = chave;
	for (; *texto; texto++) {
		*texto = subtrairLetras(*texto, *ciclagemChave);
		ciclagemChave++;
		if (!*ciclagemChave) ciclagemChave = chave;
	}
}

int main(int argc, char** argv) {
	const char* AJUDA = "1⁰ Argumento -> Tamanho da chave\n"
                        "2⁰ Argumento -> Texto encriptado";

	if (argc < 3) {
		printf("Argumentos insuficientes!\n");
		printf("%s\n", AJUDA);
		return 1;
	}

	int tamanhoChave = atoi(argv[1]);
	if (tamanhoChave <= 0) {
		printf("%d não é um tamanho de chave válido!\n", tamanhoChave);
		printf("%s\n", AJUDA);
		return 2;
	}

	char* textoEncriptado = argv[2];

	for (char* ajustarTexto = textoEncriptado; *ajustarTexto; ) {
		*ajustarTexto = tolower(*ajustarTexto);
		// retirar o char se for diferente de uma letra
		if (*ajustarTexto < 'a' || *ajustarTexto > 'z') {
			for (char* i = ajustarTexto; *i; i++) *i = *(i + 1);
			continue; // pra não cair no ajustarTexto++
		}

		ajustarTexto++;
	}
	printf("O texto encriptado é:\n%s\n\n", textoEncriptado);

	char* chaveDecifrada = quebrarChaveAgoraDeVdd(textoEncriptado, tamanhoChave);

	printf("A chave decifrada é -> %s\n\n", chaveDecifrada);

	decifrarTextoAPartirDaChave(textoEncriptado, chaveDecifrada);

	printf("O texto plano decifrado é:\n%s\n\n", textoEncriptado);
	free(chaveDecifrada);

	return 0;
}
