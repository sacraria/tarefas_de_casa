#include <stdio.h>
#include <stdbool.h>

int main() {
   float quantiaGasolina, preçoGasolina;
   float descontoAplicado = 0;
   bool isFirstRun = 1; // para a quantia de gasolina

   do {
      if (isFirstRun == 0) {
         printf("A quantia tem que ser no mínimo 10!\n");
      }
      printf("Escreva a quantia em litros de gasolina inserida -> ");
      scanf("%f", &quantiaGasolina);
      isFirstRun = 0;
   } while (quantiaGasolina < 10);

	printf("Escreva o preço atual da gasolina -> ");
	scanf("%f", &preçoGasolina);

	if (quantiaGasolina >= 10 && quantiaGasolina <= 20) {
   	descontoAplicado = 0.9; // 100% - 10% = 90%
	} else {
   	descontoAplicado = 0.8;
	}
	float *preçoFinal = &preçoGasolina;
	*preçoFinal = quantiaGasolina * preçoGasolina * descontoAplicado;
	printf("O preço final da gasolina é R$%.2f\n", *preçoFinal); 

	return 0;
}
