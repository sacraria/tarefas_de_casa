#include <stdio.h>

int main() {
   double input;
   printf("Digite o número -> ");
   scanf("%lf", &input);

   if (input == 0) {
      printf("O número é nulo\n");
   } else if (input <  0) {
      printf("O número é negativo\n");
   } else {
      printf("O número é positivo\n");
   }
   return 0;
}
