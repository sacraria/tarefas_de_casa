#include <stdio.h>

int main() {
   int a, b, x;
   printf("Escreva o primeiro número -> ");
   scanf("%d", &a);
   printf("Escreva o segundo número -> ");
   scanf("%d", &b);
   x = a + b;

   if (x >= 10) {
      printf("%d\n", x);
   }
   return 0;
}
