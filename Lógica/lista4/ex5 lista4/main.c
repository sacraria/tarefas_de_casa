#include <stdio.h>
#include <stdlib.h>

int verificaçãoNota (float x) {
	if ((x * 10 - (int)(x * 10) != 0 || x > 10 || x < 0)) {
   	printf("A nota precisa ter apenas um dígito após a vírgula e estar entre 0 e 10!\n");
   	exit(1);
	}
	return 0;
}

int main() {
   float nota1, nota2, nota3, nota4, output; 
   printf("Digite as notas do aluno, respectivamente:\n");
   scanf("%f", &nota1);
   verificaçãoNota(nota1);
   scanf("%f", &nota2);
   verificaçãoNota(nota2);
   scanf("%f", &nota3);
   verificaçãoNota(nota3);
   scanf("%f", &nota4);
   verificaçãoNota(nota4);

   output = ((nota1 + nota2 + nota3 + nota4) / 4.0);

	if (output >= 6) {
   	printf("Aprovado\n");
	} else {
   	printf("Reprovado\n");
	}
	printf("média -> %0.1f\n", output);

	return 0;
}
