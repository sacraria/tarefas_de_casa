#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void choiceTable() {
   printf("1 - Soma\n");
   printf("2 - Subtração\n");
   printf("3 - Multiplicação\n");
   printf("4 - Divisão\n");
   printf("5 - Módulo\n");
}

void choiceLoop(unsigned short *choice) {
   bool isFirstRun = 0;
   do {
      if (isFirstRun == 0) {
         isFirstRun = 1;
      } else {
         printf("Número inválido!\n");
      }
      printf("Digite o número correspondente a operação:\n");
      printf("\n");
      choiceTable();
      printf("\n");
      scanf("%hu", choice); 
      printf("\n");
   } while ( *choice > 5 || *choice < 1 );
}

int main() {
   long double input1, input2, operation;
   unsigned short choice;

   printf("Digite o primeiro número -> ");
   scanf("%Lf", &input1);
   printf("Digite o segundo número -> ");
   scanf("%Lf", &input2);
   printf("\n");

   choiceLoop(&choice);

	switch (choice) {
		case 1:
   		operation = input1 + input2;
   		break;
   	case 2:
      	operation = input1 - input2;
      	break;
      case 3:
         operation = input1 * input2;
         break;
      case 4:
         if (input2 == 0) {
            operation = 1;
         } else {
            operation = input1 / input2;
         }
         break;
      case 5:
         if (input2 == 0) {
         	printf("Não é possível obter o módulo de %Lf com 0!\n", input1);
         	exit(1);
         } else {
            static const long double AFTER_COMMA = 1000000;
         	operation = (long)(input1 * AFTER_COMMA) % (long)(input2 * AFTER_COMMA) / (long double)(AFTER_COMMA);
         }
         break;
	}

	printf("Aqui seu resultado -> %Lf\n", operation);

	return 0;
}
