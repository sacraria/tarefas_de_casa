#include <stdio.h>
#include <stdbool.h>

int choiceTable(unsigned int *choice, bool *checkFirstRun) {
   if (*checkFirstRun != 1) {
      printf("O número precisa estar entre 1 e 4!\n");
   }
   printf("Digite o número correspondente ao estado:\n\n");
   printf("1 - Minas Gerais\n");
   printf("2 - São Paulo\n");
   printf("3 - Rio de Janeiro\n");
   printf("4 - Mato Grosso do Sul\n\n");
   if (scanf("%ud", choice) != 1) {
      *choice = 0;
      while (getchar() != '\n');
   }
   *checkFirstRun = 0;
   return *choice;
}

int main() {
   unsigned int choice;
   long double Cash;
   bool checkFirstRun = 1;
   printf("Digite a grana -> ");
   scanf("%Lf", &Cash);
   // if (scanf("%Lf", &Cash) != 1) {
   //    printf("Digita certo doido\n");
   //    return 1;
   // }

   do {
   choiceTable(&choice, &checkFirstRun);
   } while (choice > 4 || choice < 1);

	long double *finalPrice = &Cash;

	switch (choice) {
		case 1:
   		*finalPrice *= 1.07;
   		break;
   	case 2:
      	*finalPrice *= 1.12;
      	break;
      case 3:
         *finalPrice *= 1.15;
         break;
      case 4:
         *finalPrice *= 1.08;
         break;
      default:
         printf("CU\n");
         return 1;
	}

	printf("Aqui ó -> %.2Lf\n", *finalPrice);

	return 0;
}
