#include <stdio.h>

int main() {
   double input;
   unsigned char num;

   printf("Digite o número -> ");
   scanf("%lf", &input);
   if (input <= 3) {
      num = 2;
   } else {
      num = 3;
   }
   input = input * (double)num;
   printf("Aqui ó -> %lf\n", input);
   return 0;
}
