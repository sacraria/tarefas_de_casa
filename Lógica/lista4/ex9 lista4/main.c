#include <stdio.h>

int main() {
   double input1, input2;
   printf("informe o primeiro número -> ");
   scanf("%lf", &input1);
   printf("informe o segundo número -> ");
   scanf("%lf", &input2);

   if (input1 == input2) {
      printf("São enguais\n");
   } else if (input1 > input2) {
      printf("O primeiro é maior\n");
   } else {
      printf("O segundo é maior\n");
   }
}
