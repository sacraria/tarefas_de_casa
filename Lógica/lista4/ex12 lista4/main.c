#include <stdio.h>

long double baseCheck(long double *x) {
   *x = -1; //pre assign a non compatible number
   do {
      scanf("%Lf", x);
      if (*x < 0) {
      	printf("O número deve ser maior que 0!\n");
      }
   } while (*x < 0);
   return *x;
}

int main() {
   long double baseMaior, baseMenor;
   printf("Digite a base maior -> ");
   baseCheck(&baseMaior);
   printf("Digite a base menor -> ");
   baseCheck(&baseMenor);
   baseMaior = (baseMaior / baseMenor) / 3.0L;
      printf("Aqui o resultado ó -> %Lf\n", baseMaior);
}
