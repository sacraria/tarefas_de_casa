#include <stdio.h>

int main() {
   unsigned short num;
   printf("Digite o número meu consagrado -> ");
   scanf("%hi", &num);

	if (num > 9 || num < 0) {
   	printf("O número deve ser inteiro e estar entre 0 e 9!\n");
   	return 1;
	} else if (num == 0) {
      printf("zero\n");
   } else if (num == 1) {
      printf("um\n");
   } else if (num == 2) {
      printf("dois\n");
   } else if (num == 3) {
      printf("três\n");
   } else if (num == 4) {
      printf("quatro\n");
   } else if (num == 5) {
      printf("cinco\n");
   } else if (num == 6) {
      printf("seis\n");
   } else if (num == 7) {
      printf("sete\n");
   } else if (num == 8) {
      printf("oito\n");
   } else if (num == 9) {
      printf("nove\n");
   }
   return 0;
}
