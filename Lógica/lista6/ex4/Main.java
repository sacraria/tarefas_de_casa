import java.util.Scanner;

class Main {
  public static void main(String[] args) {
	  Scanner sc = new Scanner (System.in);
	  float alturaPessoa1, alturaPessoa2;
	  System.out.printf("Digite a altura da pessoa 1 -> ");
	  alturaPessoa1 = sc.nextFloat();
	  System.out.printf("Digite a altura da pessoa 2 -> ");
	  alturaPessoa2 = sc.nextFloat();

	  if (alturaPessoa1 != alturaPessoa2) {
		  if (alturaPessoa1 > alturaPessoa2) {
			  System.out.printf("a pessoa 1 é mais alta!!\n");
		  } else {
			  System.out.printf("a pessoa 2 é mais alta!!\n");
		  }
	  } else {
		 System.out.printf("Elas são iguais!!!\n");
	  }
	  sc.close();
  }
}
