import java.util.Scanner;

class Main {
	
	public static boolean verifyDiv(double num, double divNumber) {
		boolean isDiv;
		if (num % divNumber == 0) {
			isDiv = true;
		} else {
			isDiv = false;
		}
		return isDiv;
	}
	
	
	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		double num, divNumber;
		boolean isDiv;
		System.out.printf("Digite o número que quer verificar se é divisível -> ");
		num = sc.nextDouble();
		System.out.printf("Digite o número à dividir -> ");
		divNumber = sc.nextDouble();

		isDiv = verifyDiv(num, divNumber);
		if (isDiv == true) {
			System.out.printf("%f \033[1;32mé divisível\033[0m por %f!!!\n", num, divNumber);
		} else {
			System.out.printf("%f \033[1;31mnão é divisível\033[0m por %f :(\n", num, divNumber);
		}
		sc.close();
	}
}
