import java.util.Scanner;

class Main {
	public static void main (String[] args) {
		Scanner sc = new Scanner (System.in);
		System.out.printf("Digite o número: ");
		double num = sc.nextDouble();

		if (num != 0) {
			if (num > 0) {
				System.out.println("O número é positivo.");
			} else {
				System.out.println("O número é negativo.");
			}
		} else {
			System.out.println("O número é nulo.");
		}
	}
}
