import java.util.Scanner;

class Main {
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		int idade1, idade2, idade3;
		System.out.printf("Digite a idade da primeira pessoa -> ");
		idade1 = sc.nextInt();
		System.out.printf("Digite a idade da segunda pessoa -> ");
		idade2 = sc.nextInt();
		System.out.printf("Digite a idade da terceira pessoa -> ");
		idade3 = sc.nextInt();

		if (idade1 <= idade2 && idade1 <= idade3) {
   		System.out.printf("%d\t", idade1);
   		if (idade2 <= idade3) {
      		System.out.printf("%d\t", idade2);
      		System.out.printf("%d\t", idade3);
   		} else {
      		System.out.printf("%d\t", idade3);
      		System.out.printf("%d\t", idade2);
   		}
		} else if (idade2 <= idade1 && idade2 <= idade3) {
   		System.out.printf("%d\t", idade2);
   		if (idade1 <= idade3) {
      		System.out.printf("%d\t", idade1);
      		System.out.printf("%d\t", idade3);
   		} else {
      		System.out.printf("%d\t", idade3);
      		System.out.printf("%d\t", idade1);
   		}
		} else {
   		System.out.printf("%d\t", idade3);
   		if (idade1 <= idade2) {
      		System.out.printf("%d\t", idade1);
      		System.out.printf("%d\t", idade2);
   		} else {
      		System.out.printf("%d\t", idade2);
      		System.out.printf("%d\t", idade1);
   		}
		}
		System.out.printf("\n");
	}
}
