import java.util.Scanner;

class Main {
	public static void main (String[] args) {
		int temp;
		Scanner sc = new Scanner (System.in);
		System.out.printf("Digite a temperatura: ");
		temp = sc.nextInt();
		if (temp <= 3 && temp >= -3) {
			System.out.println("Temperatura adequada");
		} else {
			System.out.println("Temperatura inadequada");
		}
		sc.close();
	}
}
