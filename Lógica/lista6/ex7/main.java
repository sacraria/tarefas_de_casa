import java.util.Scanner;

class Main {
	public static void main(String[] args) {
		float media = 0;
		Scanner sc = new Scanner(System.in);
		float[] nota = new float[4];
		for (int i = 0; i <= 3; ++i) {
			do {
				System.out.printf("Digite nota %d -> ", i+1);
				nota[i] = sc.nextFloat();
			} while (nota[i] > 10 || nota[i] < 0);
		}
		media = ((nota[0] + nota[1] + nota[2] + nota[3]) / 4);
		if (media >= 7) {
			System.out.printf("\033[1;32mAPROVADO\033[0m\n");
		} else {
			if (media <= 3) {
				System.out.printf("\033[1;31mREPROVADO\033[0m\n");
			} else {
				System.out.printf("\033[1;33mEXAME\033[0m\n");
			}
		}
		sc.close();
	}
}
