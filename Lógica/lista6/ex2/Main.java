import java.util.Scanner;

class Main {
	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		System.out.printf("Digite a temperatura: ");
		int temp = sc.nextInt();

		if (temp >= -3) {
			if (temp <= 3) {
				System.out.println("Temperatura adequada");
			} else {
				System.out.println("Temperatura muito elevada");
			}
		} else {
			System.out.println("Temperatura muito baixa");
		}
	}
}
