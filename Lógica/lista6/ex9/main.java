import java.util.Scanner;

class Main {

	public static void calculos (int diarias, float preco, float desconto) {
		preco = diarias * preco;
		System.out.printf("Aqui o valor sem o desconto -> R$%.2f\n", preco);
		desconto *= preco;
		System.out.printf("Aqui o valor do desconto -> R$%.2f\n", desconto);
		preco -= desconto;
		System.out.printf("Aqui o valor final -> R$%.2f\n", preco); 
	}

	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		int tipoApartamento, diarias;
		float preco, desconto;

		do {
			System.out.printf("1 - padrão\n");
			System.out.printf("2 - luxo\n");
			System.out.printf("Digite o número correspondente -> ");
			tipoApartamento = sc.nextInt();
		} while (tipoApartamento != 1 && tipoApartamento != 2);

		System.out.printf("Agora digite as diárias -> ");
		diarias = sc.nextInt();

		if (tipoApartamento == 1) {
			preco = 139;
   		if (diarias <= 3) {
				desconto = 0.03f;
   		} else {
				desconto = 0.05f;
   		}
		} else {
			preco = 199;
			if (diarias <= 3) {
				desconto = 0.04f;
			} else {
				desconto = 0.06f;
			}
		}

		calculos(diarias, preco, desconto); 
		sc.close();
	}
}
