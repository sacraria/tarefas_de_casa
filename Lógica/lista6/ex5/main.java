import java.util.Scanner;

class Main {
	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		System.out.printf("digite a idade da pessoa -> ");

		int idade = sc.nextInt();
		if (idade >= 16) {
			if (idade <= 17 && idade >= 16 || idade > 65) {
				System.out.printf("Voto facultativo\n");
			} else {
				System.out.printf("Voto obrigatório\n");
			}
		} else {
			System.out.printf("Muito pequeno\n");
		}
		sc.close();
	}
}
