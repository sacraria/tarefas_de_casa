import torch
from transformers import Wav2Vec2ForCTC as melatonina

model = melatonina.from_pretrained("facebook/wav2vec2-base-960h")
model.eval()

poggers = torch.jit.script(model)
poggers.save("poggers.pt")
