#include <math.h>
#include <stdio.h>
#include <stdlib.h>

long double
f(long double x)
{
    return 3 * x - 1;
}

int
main(int argc, char* argv[])
{
    const int precision = (argc > 1) ? abs(atoi(argv[1])) : 10;

    long double a, b;
    for (int i = -1000; i <= 1000; i++) {
        if (f(i) * f(i + 1) < 0) {
            a = i;
            b = i + 1;
        }
    }

    long double x = (a + b) / 2;
    long double xOld = x;

    do {
        xOld = x;
        if (f(x) * f(a) < 0) {
            b = x;
            x = (x + a) / 2;
        } else {
            a = x;
            x = (x + b) / 2;
        }
        printf("%.20Lf\n", x);
    } while (((x > xOld) ? x - xOld : xOld - x) > pow(10, -precision));
}
