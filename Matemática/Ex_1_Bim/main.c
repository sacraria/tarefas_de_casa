#include <stdio.h>

int main() {
    int linhasA;
    int colunasA;
    int i, j, k, l;
    printf("Digite a quantia de linhas da primeira matriz: ");
    scanf("%d", &linhasA);
    printf("Digite a quantia de colunas da primeira matriz: ");
    scanf("%d", &colunasA);
    double matrizA[linhasA][colunasA];

    for (i = 0; i < linhasA; i++) {
        for (int j = 0; j < colunasA; j++) {
            printf("Na linha %d e coluna %d, digite o valor: ", i + 1, j + 1);
            scanf("%lf", &matrizA[i][j]);
        }
    }

    int linhasB;
    int colunasB;
    printf("Digite a quantia de linhas da segunda matriz: ");
    scanf("%d", &linhasB);
    printf("Digite a quantia de colunas da segunda matriz: ");
    scanf("%d", &colunasB);
    double matrizB[linhasB][colunasB];

    if (colunasA != linhasB) {
        printf("As matrizes não são compatíveis!\n");
        return 1;
    }

    for (i = 0; i < linhasB; i++) {
        for (int j = 0; j < colunasB; j++) {
            printf("Na linha %d e coluna %d, digite o valor: ", i + 1, j + 1);
            scanf("%lf", &matrizB[i][j]);
        }
    }

    double matrizC[linhasA][colunasB];
    double sum;
    for (i = 0; i < linhasA; i++) {
        for (j = 0; j < colunasB; j++) {
            sum = 0;
            for (k = 0; k < colunasA; k++) {
                sum += matrizA[i][k] * matrizB[k][j];
            }
            matrizC[i][j] = sum;
        }
    }

    for (i = 0; i < linhasA; i++) {
        printf("( ");
        for (j = 0; j < colunasB; j++) {
            printf("%lf%c", matrizC[i][j], (j < colunasB - 1) ? '\t': '\0');
        }
        printf(" )\n");
    }
}
