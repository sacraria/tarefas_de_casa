#include "teste.h"
#include <stdio.h>
#include <stdbool.h>

void fazer(char** coiso) {
    printf("feito %s\n", *coiso);
}

void fazerAlmoco(char** comida) {
    fazer(&*comida);
}

void isEmo(struct pou2* jorge) {
    if (jorge->pouFelicidade > 2) {
        printf("Não é emo!\n");
    } else {
        printf("É emo!\n");
    }
}
