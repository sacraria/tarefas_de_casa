#include <stdio.h>

int main() {
   static const float valorMensal = 21.4;
   static const float valorMinutoUrbano = 0.03;
   static const float valorMinutoInterUrbano = 0.4;

	float minutosLocais, minutosInterUrbanos, valorTotal;
	printf("Insira os minutos urbanos -> ");
	scanf("%f", &minutosLocais);
	printf("Insira os minutos inter urbanos -> ");
	scanf("%f", &minutosInterUrbanos);

	if (minutosLocais > 60) {
		valorTotal += minutosLocais * valorMinutoUrbano;
	}
	if (valorMinutoInterUrbano > 60) {
   	valorTotal += minutosInterUrbanos * valorMinutoInterUrbano;
	}
	valorTotal += valorMensal;

	printf("R$%.2f\n", valorTotal);
}
