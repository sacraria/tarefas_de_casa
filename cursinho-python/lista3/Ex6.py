lista_maneira = []
for i in range(0, 10):
	lista_maneira.append(float(input("Digite o número de índice %d -> " % i)))


is_positivo = True
lista_indice_par = []
lista_indice_impar = []
for i in range(0, 10):
	if is_positivo:
		lista_indice_par.append(lista_maneira[i])
	else:
		lista_indice_impar.append(lista_maneira[i])
	is_positivo = not is_positivo

print("Valores dentro dos indices par: ")
for i in lista_indice_par:
	print(i)

print("Valores dentro dos indices impar: ")
for i in lista_indice_impar:
	print(i)
