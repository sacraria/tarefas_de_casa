media = float(input("Digite a média das notas: "))
notas = []

while True:
	nota_maneira = input(("Digite uma nota (ou \"sair\" para sair) -> "))
	if nota_maneira == "sair":
		break
	nota_maneira = float(nota_maneira)
	notas.append(nota_maneira)

print("Notas acima da média:")
for i in notas:
	if i >= media:
		print(i)
