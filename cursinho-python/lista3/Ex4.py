pares = []
impares = []

for i in range(1, 21):
	num = int(input("Digite o %d número (inteiro) -> " % i))
	if num % 2 == 0:
		pares.append(num)
	else:
		impares.append(num)


print("pares:")
for i in pares:
	print(i)

print()

print("impares:")
for i in impares:
	print(i)

print()
