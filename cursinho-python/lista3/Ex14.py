ano_atual = int(input("Digite o ano atual -> "))
anos_campeao = []
while True:
	ano = input("Digite um ano em que o Brasil foi campeão (ou \"sair\" para sair) -> ")
	if ano == "sair":
		break
	anos_campeao.append(int(ano))

print("Se passaram %d anos da última conquista até o ano atual" % (ano_atual - anos_campeao[-1]))

for index in range(1, len(anos_campeao)):
	print("Se passaram %d anos entre a conquista de %d e %d" %
		(anos_campeao[index] - anos_campeao[index - 1], anos_campeao[index - 1], anos_campeao[index]))
