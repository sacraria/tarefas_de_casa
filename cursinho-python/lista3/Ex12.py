nome_produtos = []
preco_produtos = []

while True:
	nome_produto = input("Digite o nome de um produto (ou \"sair\" para sair) -> ")
	if nome_produto == "sair":
		break
	nome_produtos.append(nome_produto)
	preco_produtos.append(float(input("Digite o preço desse produto -> ")))

produtos_mais_caros_index = []
for _ in range(min(3, len(preco_produtos))):
	maior_index = 0
	for index in range(1, len(preco_produtos)):
		if preco_produtos[index] > preco_produtos[maior_index] and index not in produtos_mais_caros_index:
			maior_index = index
	produtos_mais_caros_index.append(maior_index)

print("Três produtos mais caros:")
for index in produtos_mais_caros_index:
	print("Nome: %s, preço: %.2f" % (nome_produtos[index], preco_produtos[index]))

