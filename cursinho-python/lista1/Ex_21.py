valorCompra = float(input("Digite o valor da compra em dólar: "))
taxaCambio = float(input("Digite o valor da taxa de câmbio atual: "))

valorReais = valorCompra * taxaCambio
print("Valor da compra em reais -> R$%.2f" % (valorReais))
print("Valor do IOF -> R$%.2f" % (valorReais * 0.0638))
print("Valor total da compra -> R$%.2f" % (valorReais * 1.0638))
