assinatura = 21.4
print("Valor da assinatura -> R$%.2f" % assinatura)

minutosLocaisExcedentes = float(input("Digite os minutos locais usados: "))
minutosLocaisExcedentes -= 60
if minutosLocaisExcedentes < 0: minutosLocaisExcedentes = 0
valorMinutosLocais = minutosLocaisExcedentes * 0.03
print("Valor de minutos locais -> R$%.2f" % valorMinutosLocais)

minutosInterurbanosExcedentes = float(input("Digite os minutos interurbanos usados: "))
minutosInterurbanosExcedentes -= 30
if minutosInterurbanosExcedentes < 0: minutosInterurbanosExcedentes = 0
valorMinutosUrbanos = minutosInterurbanosExcedentes * 0.4
print("Valor de minutos interurbanos -> R$%.2f" % valorMinutosUrbanos)

print("Valor total à ser pago -> R$%.2f" % (assinatura + valorMinutosLocais + valorMinutosUrbanos))
