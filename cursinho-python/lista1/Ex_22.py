from math import sqrt

xa = float(input("Digite a coordernada X do ponto A: "))
ya = float(input("Digite a coordernada Y do ponto A: "))
xb = float(input("Digite a coordernada X do ponto B: "))
yb = float(input("Digite a coordernada Y do ponto B: "))

distancia = sqrt((xb - xa) ** 2 + (yb - ya) ** 2)

print("A distância entre os dois pontos é -> %f" % distancia)
