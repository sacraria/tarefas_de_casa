if int(input("Qual o tipo de média? (0 - Simples, 1 - Ponderada): ")) > 0:
	media = 0.0
	pesoTotal = 0.0
	for i in range(1, 4):
		peso = float(input("Digite o peso do %d número: " % i))
		pesoTotal += peso
		media += float(input(("Digite o %d⁰ número: " % i))) * peso
	media /= pesoTotal

else:
	media = 0.0
	for i in range(1, 4):
		media += float(input(("Digite o %d⁰ número: " % i)))
	media /= 3.0

print("Média -> %f" % media)
