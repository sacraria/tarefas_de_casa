nome1 = input("Digite o nome da primeira pessoa: ")
altura1 = float(input("Digite a altura da primeira pessoa: "))
peso1 = float(input("Digite o peso da primeira pessoa: "))

nome2 = input("Digite o nome da segunda pessoa: ")
altura2 = float(input("Digite a altura da segunda pessoa: "))
peso2 = float(input("Digite o peso da segunda pessoa: "))

if altura1 > altura2:
	print("%s é a pessoa mais alta" % nome1)
elif altura1 < altura2:
	print("%s é a pessoa mais alta" % nome2)
else:
	print("As duas pessoas possuem a mesma altura")

if peso1 > peso2:
	print("%s é a pessoa mais pesada" % nome1)
elif peso1 < peso2:
	print("%s é a pessoa mais pesada" % nome2)
else:
	print("As duas pessoas possuem o mesmo peso")
