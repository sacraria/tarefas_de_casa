estadia = int(input("Estadia de quantos dias? "))
valorTotal = 0

if estadia < 5:
	valorTotal = estadia * (60.0 + 8.0)
elif estadia > 5:
	valorTotal = estadia * (60.0 + 5.5)
else:
	valorTotal = estadia * (60.0 + 6.0)

print("O valor total da estadia é %f" % valorTotal)
