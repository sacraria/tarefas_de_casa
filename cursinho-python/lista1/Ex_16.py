from math import sqrt

cateto1 = float(input("Digite o comprimento do 1⁰ cateto: "))
cateto2 = float(input("Digite o comprimento do 2⁰ cateto: "))

hipotenusa = sqrt(cateto1 ** 2 + cateto2 ** 2)

print("hipotenusa -> %f" % hipotenusa)
