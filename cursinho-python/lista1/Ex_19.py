quantiaDinheiro = int(input("Digite a quantia de dinheiro para o caixa deve dar: "))

count = 0
while (quantiaDinheiro >= 200):
	count += 1
	quantiaDinheiro -= 200

while (quantiaDinheiro >= 100):
	count += 1
	quantiaDinheiro -= 100

while (quantiaDinheiro >= 50):
	count += 1
	quantiaDinheiro -= 50

while (quantiaDinheiro >= 20):
	count += 1
	quantiaDinheiro -= 20

while (quantiaDinheiro >= 10):
	count += 1
	quantiaDinheiro -= 10

while (quantiaDinheiro >= 5):
	count += 1
	quantiaDinheiro -= 5

while (quantiaDinheiro >= 2):
	count += 1
	quantiaDinheiro -= 2

while (quantiaDinheiro >= 1):
	count += 1
	quantiaDinheiro -= 1


print("O caixa deve dar no mínimo %d notas" % count)
