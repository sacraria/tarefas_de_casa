num1 = float(input("Digite o 1⁰ número: "))
num2 = float(input("Digite o 2⁰ número: "))
num3 = float(input("Digite o 3⁰ número: "))

maiorValor = num1
maior = 1
if num2 > maiorValor:
	maiorValor = num2
	maior = 2
if num3 > maiorValor:
	maiorValor = num3
	maior = 3

print("O %d⁰ número, com o valor de %f, é o maior" % (maior, maiorValor))
