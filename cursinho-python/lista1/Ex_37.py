notaPortugues = float(input("Digite a nota do aluno em Português (de 0 à 10): "))
notaMatematica = float(input("Digite a nota do aluno em Matemática (de 0 à 10): "))
notaConhecimentosGerais = float(input("Digite a nota do aluno em Conhecimentos Gerais (de 0 à 10): "))

media = (notaPortugues + notaMatematica + notaConhecimentosGerais) / 3
print("A média do aluno é %f" % media)

if media >= 7 and not(notaPortugues < 5 or notaMatematica < 5 or notaConhecimentosGerais < 5):
	print("Parabéns passou")
else:
	print("reprovou")
