alturaCaixa = float(input("Digite a altura do modelo da caixa: "))
comprimentoCaixa = float(input("Digite o comprimento do modelo da caixa: "))
larguraCaixa = float(input("Digite a largura do modelo da caixa: "))
quantiaCaixas = float(input("Digite a quantidade de caixas à serem produzidas: "))

quantidadeMaterialProduzir = ((comprimentoCaixa * larguraCaixa * 2) + (alturaCaixa * larguraCaixa * 2) + (alturaCaixa * comprimentoCaixa * 2)) * 1.1

print("%f é a quantidade de material necessário para produzir em metros quadrados" % quantidadeMaterialProduzir)
