nota = int(input("Digite a nota do aluno, de 0 a 100: "))

if nota < 60:
	conceito = 'D'
elif nota < 80:
	conceito = 'C'
elif nota < 90:
	conceito = 'B'
elif nota <= 100:
	conceito = 'A'
else:
	print("Ta meio alta essa nota aí")
	exit(1)

print("A nota do aluno é %s" % conceito)
