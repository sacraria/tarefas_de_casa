custoProducao = float(input("Digite o custo de produção do produto: "))
custoTransporte = float(input("Digite o custo de transporte do produto: "))
taxaImpostos = float(input("Digite o valor dos impostos aplicados ao produto (em %): "))
margemLucro = float(input("Digite a margem de lucro do produto (em %): "))

valorFinalProduto = custoProducao + (custoProducao * (taxaImpostos / 100)) + (custoProducao * (margemLucro / 100)) + custoTransporte

print("%f é o valor final do produto" % valorFinalProduto)
