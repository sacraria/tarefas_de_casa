nomeFuncionario = input("Qual o nome do funcionário? ")
salarioBruto = float(input("Qual o salário bruto do funcionário? "))
numDependentes = float(input("Qual o número de dependentes do funcionário? "))

if salarioBruto <= 300:
	descontoINSS = salarioBruto * 0.08
elif salarioBruto > 700:
	descontoINSS = salarioBruto * 0.1
else:
	descontoINSS = 0.09

salarioLiquido = salarioBruto - descontoINSS + (15.0 * numDependentes) + 40.0 + 100.0

print("R$%.2f é o salário líquido do funcionário" % salarioLiquido)
