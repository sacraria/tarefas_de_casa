n = int(input("Digite quantos números da série de Fetucine: "))

if n < 3:
	print("São necessários pelo menos 3 termos!!!")
	exit(1)

num_menor = float(input("Digite o primeiro termo: "))
num_maior = float(input("Digite o segundo termo: "))
mais = True

print("%d" % num_menor)
print("%d" % num_maior)

for i in range(n - 2):
	tmp = 0
	if mais:
		tmp = num_maior + num_menor
	else:
		tmp = num_maior - num_menor
	num_menor = num_maior
	num_maior = tmp
	mais = not mais
	print("%d" % num_maior)
