numMenor = int(input("Digite o número menor: "))
numMaior = int(input("Digite o número maior: "))

if numMaior <= numMenor:
    print("O número deve ser menor que o outro!!!")
    exit()

print("Números pares entre eles:")
for i in range(numMenor, numMaior + 1):
    if i % 2 == 0:
        print(i)
