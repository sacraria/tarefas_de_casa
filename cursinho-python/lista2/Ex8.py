lista_legal = []
while True:
	resposta = input("Digite um número inteiro (ou \"sair\" para sair): ")
	if resposta == "sair":
		break

	lista_legal.append(int(resposta))


soma = 0
for i in lista_legal:
	if i % 2 == 0:
		soma += i

print("soma dos números pares -> %d" % soma)
