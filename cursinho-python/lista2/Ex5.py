numMaior = int(input("Digite o número maior: "))
numMenor = int(input("Digite o número menor: "))

if numMaior <= numMenor:
    print("O número deve ser menor que o outro!!!")
    exit()


print("Números de forma decrescente:")
for i in range(numMaior, numMenor -1, -1):
    print(i)
