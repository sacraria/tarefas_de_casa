media = 0
num_iteracoes = 0
while True:
	num = float(input("Digite um número (negativo para sair): "))
	if num < 0:
		break
	media += num
	num_iteracoes += 1

media /= num_iteracoes

print("Média -> %f" % media)
