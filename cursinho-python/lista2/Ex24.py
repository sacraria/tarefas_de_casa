n = int(input("Digite n: "))

soma = 0
for i in range(1, n):
	if n % i == 0:
		soma += i

if soma == n:
	print("%d é um número perfeito" % n)
else:
	print("%d não é um número perfeito" % n)
