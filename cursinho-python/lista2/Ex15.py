quantia_pessoas_abaixo_peso = 0
for i in range(1, 11):
	peso = float(input("Digite o peso em Kg da %d⁰ pessoa: " % i))
	altura = float(input("Digite a altura em metros da %d⁰ pessoa: " % i))
	if (peso / altura) ** 2 <= 18.5:
		quantia_pessoas_abaixo_peso += 1

print("%d pessoas estão abaixo do peso" % quantia_pessoas_abaixo_peso)
