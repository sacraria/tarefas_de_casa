num_1 = int(input("Digite o 1⁰ número: "))
num_2 = int(input("Digite o 2⁰ número: "))

num_menor = 0
if num_1 < num_2:
	num_menor = num_1
else:
	num_menor = num_2

mdc = 0
for i in range(num_menor, 0, -1):
	if num_1 % i == 0 and num_2 % i == 0:
		mdc = i
		break

print(mdc)
