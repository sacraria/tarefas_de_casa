def trocar_posicoes(lista):
	for i in range(0, len(lista) // 2):
		menos_i = i - (i * 2) - 1
		aux = lista[i]
		lista[i] = lista[menos_i]
		lista[menos_i] = aux
	return lista

print("caso 1:")
lista = trocar_posicoes([5, 4, 3, 2, 1, 0])
print(lista[:])

print("caso 2:")
lista = trocar_posicoes([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
print(lista[:])

print("caso 3:")
lista = trocar_posicoes([])
print(lista[:])
