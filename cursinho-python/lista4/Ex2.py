def numeros_pares_entre(num_menor, num_maior):
	step = 0
	if (num_menor <= num_maior):
		step = 1
		num_maior += 1
	else:
		step = -1
		num_maior -= 1

	lista = []
	for i in range(num_menor, num_maior, step):
		if i % 2 == 0:
			lista.append(i)
	return lista

# caso 1:
lista = numeros_pares_entre(1, 5)
print("caso 1 return:")
print(lista[:])

lista = numeros_pares_entre(5, 1)
print("caso 2 return:")
print(lista[:])

lista = numeros_pares_entre(20, 50)
print("caso 2 return:")
print(lista[:])
