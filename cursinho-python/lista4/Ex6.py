def media_ponderada(notas, pesos):
	if (len(notas) != len(pesos)):
		return

	soma = 0
	num_dividir = 0
	for index in range(0, len(notas)):
		soma += notas[index] * pesos[index]
		num_dividir += pesos[index]

	return soma / num_dividir

print("caso 1 -> %f" % media_ponderada([2, 4], [3, 1]))
print("caso 2 -> %f" % media_ponderada([10, 2, 20], [2, 3, 1]))
print("caso 3 -> %f" % media_ponderada([2, 4, 6, 8], [5, 3, 2, 1]))
