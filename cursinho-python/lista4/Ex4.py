def contar_valores_postitivos_e_negativos(lista):
	positivos = 0
	negativos = 0
	for i in lista:
		if i < 0:
			negativos += 1
		else:
			positivos += 1
	return (positivos, negativos)

print("caso 1: positivos -> %d, negativos -> %d" % contar_valores_postitivos_e_negativos([2, 2, 2, 2, 2]))
print("caso 2: positivos -> %d, negativos -> %d" % contar_valores_postitivos_e_negativos([-2, -2, -2, -2, -2]))
print("caso 3: positivos -> %d, negativos -> %d" % contar_valores_postitivos_e_negativos([2, 2, -2, -2, -2]))
