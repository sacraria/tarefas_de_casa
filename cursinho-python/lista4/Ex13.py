def ultimo_par(lista):
	for index in range(len(lista) - 1, -1, -1):
		if lista[index] % 2 == 0:
			return index
	return -1

print("caso 1 -> %d" % ultimo_par([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]))
print("caso 2 -> %d" % ultimo_par([2, 9, 47, 2, 5, 9, 32, 63, 8, 10, 31, 0, -1, 14, 51, 63, 7, 2.2, 19, 3]))
print("caso 3 -> %d" % ultimo_par([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]))
