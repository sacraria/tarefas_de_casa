def multiplicar_lista(lista, valor_multiplicar):
	for i in range(0, len(lista)):
		lista[i] *= valor_multiplicar
	return lista

print("caso 1:")
lista = multiplicar_lista([1, 2, 3], 3)
print(lista[:])

print("caso 2:")
lista = multiplicar_lista([3, 2, 15], 5)
print(lista[:])

print("caso 1:")
lista = multiplicar_lista([5, 4, 3, 2, 1], 10)
print(lista[:])
