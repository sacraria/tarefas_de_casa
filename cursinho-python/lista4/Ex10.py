def progressao_geometrica(termo_inicial, razao, quantia_termos):
	lista = []
	for _ in range(quantia_termos):
		lista.append(termo_inicial)
		termo_inicial *= razao
	return lista

print("caso 1:" )
lista = tuple(progressao_geometrica(1, 2, 5))
print(lista[:])

print("caso 2:" )
lista = tuple(progressao_geometrica(5, 2, 5))
print(lista[:])

print("caso 3:" )
lista = tuple(progressao_geometrica(1, 5, 5))
print(lista[:])
