def numeros_entre(num_menor, num_maior):
	sum = 0;
	step = 0
	if (num_menor <= num_maior):
		step = 1
		num_maior += 1
	else:
		step = -1
		num_maior -= 1
	for i in range(num_menor, num_maior, step):
		print(i)
		sum += i
	return sum

# caso 1:
print("caso 1 return -> %d" % numeros_entre(1, 5))

# caso 2:
print("caso 2 return -> %d" % numeros_entre(5, 1))

# caso 3:
print("caso 3 return -> %d" % numeros_entre(20, 50))
