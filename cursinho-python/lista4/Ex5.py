def media_simples(lista):
	sum = 0
	for i in lista:
		sum += i

	return sum / len(lista)

print("caso 1 -> %f" % media_simples([2, 2, 2, 2]))

print("caso 2 -> %f" % media_simples([1, 2, 3, 4]))

print("caso 3 -> %f" % media_simples([5, 4, 10, 7]))
