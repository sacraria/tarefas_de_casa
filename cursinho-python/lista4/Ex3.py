def polegadas_para_centimetros(num_polegadas):
	return num_polegadas * 2.54

def centimetros_para_polegadas(num_centimetros):
	return num_centimetros / 2.54

# caso 1 polegadas para centimetros
print("caso 1 polegadas para centimetros return -> %f" % polegadas_para_centimetros(1))

# caso 2 polegadas para centimetros
print("caso 2 polegadas para centimetros return -> %f" % polegadas_para_centimetros(3))

# caso 3 polegadas para centimetros
print("caso 3 polegadas para centimetros return -> %f" % polegadas_para_centimetros(5))

# caso 1 centimetros para polegadas
print("caso 1 centimetros para polegadas return -> %f" % centimetros_para_polegadas(1))

# caso 2 centimetros para polegadas
print("caso 2 centimetros para polegadas return -> %f" % centimetros_para_polegadas(3))

# caso 3 centimetros para polegadas
print("caso 3 centimetros para polegadas return -> %f" % centimetros_para_polegadas(5))
