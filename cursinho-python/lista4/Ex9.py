def calcular_imc(peso_kg, altura_metros):
	imc = peso_kg / altura_metros ** 2
	return (imc, imc <= 18.5)

print("caso 1: imc -> %f, abaixo do peso -> %r" % calcular_imc(63.0, 1.76))
print("caso 2: imc -> %f, abaixo do peso -> %r" % calcular_imc(47.0, 1.50))
print("caso 3: imc -> %f, abaixo do peso -> %r" % calcular_imc(70.0, 1.80))
