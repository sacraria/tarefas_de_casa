def fatorial(num):
	fatorial = 1
	while (num > 1):
		fatorial *= num
		num -= 1
	return fatorial

print("caso 1 -> %d" % fatorial(5))
print("caso 2 -> %d" % fatorial(3))
print("caso 3 -> %d" % fatorial(7))
