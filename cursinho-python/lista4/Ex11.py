def fahrenheit_para_celsius(f):
	return (5 / 9) * (f - 32)

print("caso 1 -> %f" % fahrenheit_para_celsius(1))
print("caso 2 -> %f" % fahrenheit_para_celsius(86))
print("caso 3 -> %f" % fahrenheit_para_celsius(68))
